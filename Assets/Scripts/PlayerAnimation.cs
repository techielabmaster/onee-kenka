﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TanksMP
{
    public class PlayerAnimation : MonoBehaviour
    {
        Player player;
        Animator animator;

        void Awake()
        {
            player = GetComponent<Player>();
            animator = GetComponentInChildren<Animator>();
        }

        void FixedUpdate()
        {
            animator.SetInteger("State", (int)player.state);
        }
    }
}